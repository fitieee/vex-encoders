 int val; 
 int encoder0PinA = 3;
 int encoder0PinB = 5;
 int encoder0Pos = 0;
 int encoder0PinALast = LOW;
 int encoder0PinBLast = LOW;
 int f = LOW;
 int r = LOW;

 int cim=0;
 int Deg = 0;

 void setup() { 
   pinMode (encoder0PinA,INPUT);
   pinMode (encoder0PinB,INPUT);
   Serial.begin (9600);
 } 

 void loop() {   
   f = digitalRead(encoder0PinA);     // Read Encoder0PinA, forward rotation
   r = digitalRead(encoder0PinB);     // Read Encoder0PinB, reverse rotation
   
   if ((f == HIGH) && (encoder0PinALast == LOW) && (r == LOW)) {
     encoder0Pos++;
     //Serial.println("1:");
     Serial.println(encoder0Pos);
   } 
   if ((f == HIGH) && (encoder0PinALast == LOW) && (r == HIGH)) {
     encoder0Pos--;
     //Serial.println("2:");
     Serial.println(encoder0Pos);
   } 
   if ((r == HIGH) && (encoder0PinBLast == LOW) && (f == LOW)) {
     encoder0Pos--;
     //Serial.println("3:");
     Serial.println(encoder0Pos);
   } 
   if ((r == HIGH) && (encoder0PinBLast == LOW) && (f == HIGH)) {
     encoder0Pos++;
    // Serial.println("4:");
     Serial.println(encoder0Pos);
   } 

   if ((f == LOW) && (encoder0PinALast == HIGH) && (r == LOW)) {
     encoder0Pos--;
     //Serial.println("5:");
     Serial.println(encoder0Pos);
   } 
   if ((f == LOW) && (encoder0PinALast == HIGH) && (r == HIGH)) {
     encoder0Pos++;
     //Serial.println("6:");
     Serial.println(encoder0Pos);
   } 
   if ((r == LOW) && (encoder0PinBLast == HIGH) && (f == LOW)) {
     encoder0Pos++;
     //Serial.println("7:");
     Serial.println(encoder0Pos);
   } 
   if ((r == LOW) && (encoder0PinBLast == HIGH) && (f == HIGH)) {
     encoder0Pos--;
    // Serial.println("8:");
     Serial.println(encoder0Pos);
   } 

  
   encoder0PinALast = f;
   encoder0PinBLast = r;
   
 }



